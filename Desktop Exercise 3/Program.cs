﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var myPeople = new List<Person>();

//      using (var db = new AdventureWorks2014Entities())
//      {
//        myPeople = db.People.Take(500).OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToList();

//      }
//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q1Path = @"../../query1.txt";
//      using (var swq1 = new StreamWriter(q1Path))
//      {
//        myPeople.ForEach(x => swq1.WriteLine("{0}, {1}", x.LastName, x.FirstName));
//      }
//    }
//  }
//}

//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var myCustomers = new List<Customer>();

//      using (var db = new AdventureWorks2014Entities())
//      {
//        myCustomers = db.Customers.OrderBy(x => x.AccountNumber).ToList();


//      }
//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q1Path = @"../../QueryOne.txt";
//      using (var swq1 = new StreamWriter(q1Path))
//      {
//        myCustomers.ForEach(x => swq1.WriteLine("{0}", x.AccountNumber));
//        swq1.WriteLine("My total customer count is {0}", myCustomers.Count);
//      }
//    }
//  }
//}
//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var myCustomers = new List<Customer>();

//      using (var db = new AdventureWorks2014Entities())
//      {

//        myCustomers = db.Customers
//          .Include(x => x.Person)
//          .Include(x => x.Store)
//          .Include(x => x.SalesOrderHeaders)
//          .OrderBy(x => x.Person.LastName)
//          .ThenBy(x => x.Person.FirstName)
//          .ToList();
//      }



//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q2Path = @"../../QueryTwo.txt";
//      using (var swq2 = new StreamWriter(q2Path))
//      {
//        //customerDetails.ForEach(x => swq2.WriteLine("{0}", x));

//        foreach (var p in myCustomers)
//        {

//          swq2.WriteLine("{0}, {1}, {2}, {3}, {4}",
//            p.CustomerID,
//            (p.Person != null) ? p.Person.LastName : "N/A",
//            (p.Person != null) ? p.Person.FirstName : "",
//            p.AccountNumber,
//            (p.Store != null) ? p.Store.Name : "N/A"
//            );

//          foreach (var item in p.SalesOrderHeaders)
//          {
//            swq2.WriteLine("\t\tSubtotal {0:C}, Freight {1:C}, Total {2:C}, Tax{3:C}",
//              item.SubTotal,
//              item.Freight,
//              item.TotalDue,
//              item.TaxAmt
//          );
//          }
//        }
//        swq2.WriteLine("There are {0} customers", myCustomers.Count);

//      }
//        }
//        }
//      }
    
  

//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var productlistLambda = new List<Product>();
//      var productListLinq = new List<Product>();
//      var productStylesDistinctLambda = new List<string>();
//      var productStylesDistinctLinq = new List<string>();
//      var productWrongDistinct = new List<string>();
//      using (var db = new AdventureWorks2014Entities())
//      {
//        productlistLambda = db.Products.ToList();
//        productListLinq = (from p in db.Products select p).ToList();
//        productStylesDistinctLambda = db.Products.Select(x => x.Style).Distinct().Distinct().ToList();
//        productStylesDistinctLinq = (from p in db.Products select p.Style).Distinct().ToList();

//      }
//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q1Path = @"../../query1.txt";
//      using (var swq1 = new StreamWriter(q1Path))
//      {
//        foreach (var p in productlistLambda)
//        {
//          swq1.WriteLine("{0}, {1}, {2}, {3:C}",
//            p.ProductID,
//            p.Name,
//            (string.IsNullOrEmpty(p.Style)) ? "NO STYLE" : p.Style,
//            p.ListPrice);

//        }
//      }
//    }
//  }
//}


//Queury 3//

//{
//  class Program
//  {
//    static void Main(string[] args)
//    {
//      //inside this using statement, db is your database connection; the source of all of your queries
//      //outside of the using statement, the database connection no longer exists and is cleaned up
//      //by the garbage collector

//      var myCustomers = new List<Customer>();

//      using (var db = new AdventureWorks2014Entities())
//      {

//        myCustomers = db.Customers
//          .Include(x => x.Person)
//          .Include(x => x.Store)
//          .Include(x => x.SalesOrderHeaders)
//          .Where(x => x.Person.LastName == "Chapman")
//          .OrderBy(x => x.Person.LastName)
//          .ThenBy(x => x.Person.FirstName)
//          .ToList();
//      }



//      //always have to use a using statement, outside db no longer exists
//      //path to the output file contained in the project
//      var q3Path = @"../../Querythree.txt";
//      using (var swq3 = new StreamWriter(q3Path))
//      {
//        //customerDetails.ForEach(x => swq2.WriteLine("{0}", x));

//        foreach (var p in myCustomers)
//        {

//          swq3.WriteLine("{0}, {1}, {2}, {3}, {4}",
//            p.CustomerID,
//            (p.Person != null) ? p.Person.LastName : "N/A",
//            (p.Person != null) ? p.Person.FirstName : "",
//            p.AccountNumber,
//            (p.Store != null) ? p.Store.Name : "N/A"
//            );

//          foreach (var item in p.SalesOrderHeaders)
//          {
//            swq3.WriteLine("\t\tSubtotal {0:C}, Freight {1:C}, Total {2:C}, Tax{3:C}",
//              item.SubTotal,
//              item.Freight,
//              item.TotalDue,
//              item.TaxAmt
//          );
//          }
//        }
//        swq3.WriteLine("There are {0} customers", myCustomers.Count);

//      }
//        }
//        }
//      }


//Queryfive//
{
  class Program
  {
    static void Main(string[] args)
    {
      //inside this using statement, db is your database connection; the source of all of your queries
      //outside of the using statement, the database connection no longer exists and is cleaned up
      //by the garbage collector

      var myCustomers = new List<Customer>();

      using (var db = new AdventureWorks2014Entities())
      {

        myCustomers = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ToList();
      }



      //always have to use a using statement, outside db no longer exists
      //path to the output file contained in the project
      var q5Path = @"../../Queryfive.txt";
      using (var swq5 = new StreamWriter(q5Path))
      {
        //customerDetails.ForEach(x => swq2.WriteLine("{0}", x));

        foreach (var p in myCustomers)
        {

          swq5.WriteLine("{0}, {1}, {2}, {3}, {4}",
            p.CustomerID,
            (p.Person != null) ? p.Person.LastName : "N/A",
            (p.Person != null) ? p.Person.FirstName : "",
            p.AccountNumber,
            (p.Store != null) ? p.Store.Name : "N/A"
            );

          foreach (var item in p.SalesOrderHeaders)
          {
            swq5.WriteLine("\t\tSubtotal {0:C}, Freight {1:C}, Total {2:C}, Tax{3:C}, Percentage Freight {4:P}",
              item.SubTotal,
              item.Freight,
              item.TotalDue,
              item.TaxAmt,
              item.Freight/item.TotalDue
          );
          }
        }
        swq5.WriteLine("There are {0} customers", myCustomers.Count);

      }
    }
  }
}